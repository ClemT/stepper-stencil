
import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'se-page4',
  styleUrl: 'page4.css',
  shadow: true
})

export class Page4 {

  render() {
    return (
      <Host>
        <div>Page 4</div>
      </Host>
    )
  } 

}
