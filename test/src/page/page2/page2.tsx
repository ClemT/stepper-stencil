
import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'se-page2',
  styleUrl: 'page2.css',
  shadow: true
})

export class Page2 {

  render() {
    return (
      <Host>
        <div>Page 2</div>
      </Host>
    )
  } 

}
