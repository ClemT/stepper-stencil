
import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'se-page3',
  styleUrl: 'page3.css',
  shadow: true
})

export class Page3 {

  render() {
    return (
      <Host>
        <div>Page 3</div>
      </Host>
    )
  } 

}
