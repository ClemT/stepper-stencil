
import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'se-page1',
  styleUrl: 'page1.css',
  shadow: true
})

export class Page1 {

  render() {
    return (
      <Host>
        <div>Page 1</div>
      </Host>
    )
  } 

}
