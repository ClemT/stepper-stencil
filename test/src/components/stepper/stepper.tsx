
import { Component, Prop, State, Watch, h } from '@stencil/core';

@Component({
  tag: 'se-stepper',
  styleUrl: 'stepper.css',
  shadow: true
})

export class Stepper {

  @Prop() myArray: string;
  @State() myInnerArray: Array<any>;
  @Prop() arr;
  @Prop() current;

  componentWillLoad() {
    this.parseMyArrayProp(this.myArray);
  }

  @Watch('myArray')
  parseMyArrayProp(newValue: string) {
    if (newValue) this.myInnerArray = JSON.parse(newValue);
  }


  render() {
    return (
      <stepper>
          {this.myInnerArray.map(item => 
            <step  onClick={ (ev: UIEvent) => this.click(ev)} class={  (this.current >= item.nbr ? ' active' : ' inactive') } page={item.page} number={item.nbr} disable={item.disable}> <span>{item.name}</span> </step>
          )}
      </stepper>
    )
  }
  
  click(ev){
    let el = ev.target;
    el.setAttribute("current","true")
    this.current = el.getAttribute("number") ;
    this.replaceContent(el);
  }

  replaceContent(el){
    let wrap = document.querySelector("se-stepper-content");
    let url = el.getAttribute("page");
    let tag =   "<"+ url + "></" + url + ">";
    wrap.innerHTML = tag;
  }
}
