# my-component



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description | Type     | Default     |
| --------- | ---------- | ----------- | -------- | ----------- |
| `arr`     | `arr`      |             | `any`    | `undefined` |
| `current` | `current`  |             | `any`    | `undefined` |
| `myArray` | `my-array` |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
