
import { Component, h, Host, Listen } from '@stencil/core';

@Component({
  tag: 'se-step2',
  styleUrl: 'step2.css',
  shadow: true
})

export class Step2 {

  @Listen('click', { capture: true })
  handleClick(ev) {
    let el = ev.target;
    this.prevAll(el);
    this.nextAll(el);
    ev.target.setAttribute("active", "true");
    this.replaceContent(el);
  }

  componentDidLoad() {
    let el = document.querySelector("se-stepper2 se-step2[active='true']");
    this.prevAll(el);
  }

  render() {
    return (
      <Host>
        <span>
          <slot></slot>
        </span>
      </Host>
    )
  } 

  prevAll(elem) {
    let prev = elem.previousElementSibling;
    while (prev) {
      prev.setAttribute("active", "true");
      prev = prev.previousElementSibling
    }
  } 

  nextAll(elem) {
    let next = elem.nextElementSibling;
    while (next) {
      next.setAttribute("active", "false");
      next = next.nextElementSibling
    }
  }
  replaceContent(el){
    let wrap = document.querySelector("se-stepper2-content");
    let url = el.getAttribute("page");
    let tag =   "<"+ url + "></" + url + ">";
    wrap.innerHTML = tag;
  }



}
