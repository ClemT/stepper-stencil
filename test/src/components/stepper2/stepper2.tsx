
import { Component, h } from '@stencil/core';


@Component({
  tag: 'se-stepper2',
  styleUrl: 'stepper2.css',
  shadow: true
})


export class Stepper2 {

  render() {
    return (
      <slot ></slot>
    )
    
  }

}
